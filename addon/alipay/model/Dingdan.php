<?php
namespace addon\alipay\model;
use app\common\model\ModelBase;
/**
 * 分类模型
 */
class Dingdan extends ModelBase{
	
	protected $insert = ['create_time'=>TIME_NOW];
	protected $auto = ['update_time'=>TIME_NOW];
	protected $update = ['update_time'=>TIME_NOW];
	protected function getAddTimeAttr($value)
	{
		return friendlyDate($value);
	}
	protected function getStatusAttr($value)
	{
		if($value==1){
			$name='支付成功';
		}else{
			$name='支付失败';
		}
		return $name;
	}
	
	protected function getUidAttr($value)
	{
		
		return getusernamebyid($value);
	}


	



	
}