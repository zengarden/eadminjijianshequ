<?php
return array(
		'scorenum'=>array(
				'title'=>'兑换比例：',
				'type'=>'text',
				'value'=>'',
				'tip'=>'例如填写10表示1元人民币兑换10积分',
				'labelwidth'=>'150px',
				'width'=>'350px',
		),
		'minnum'=>array(
				'title'=>'最少充值限制：',
				'type'=>'text',
				'value'=>'',
				'tip'=>'例如填写1表示最少充值1元',
				'labelwidth'=>'150px',
				'width'=>'350px',
		),
          
);