<?php

namespace addon\alipay\logic;
use app\common\model\ModelBase;

class Alipay  extends ModelBase
{
    
   
    public static $alipayModel = null;
    
    /**
     * 构造方法
     */
    public function __construct()
    {
        
    	 $class = get_addon_model ( 'alipay',  'dingdan' );
            $model = new $class();
        
        self::$alipayModel = $model;
    }
    
    public function getDingdanInfo($where = [], $field = true)
    {
    
    	return self::$alipayModel->getInfo($where, $field);
    }
    

    public function dingdanAdd($data)
    {

        return self::$alipayModel->setInfo($data) ? [RESULT_SUCCESS, '订单添加成功'] : [RESULT_ERROR, self::$alipayModel->getError()];
    }

    public function setDingdanValue($where = [], $field = '', $value = '',$msg='')
    {
       
        return self::$alipayModel->setFieldValue($where, $field, $value) ? [RESULT_SUCCESS, $msg] : [RESULT_ERROR, self::$alipayModel->getError()];
    }

    public function dingdanAlldel($ids)
    {
    	

    return self::$alipayModel->deleteAllInfo(['id'=>array('in',$ids)]) ? [RESULT_SUCCESS, '订单删除成功'] : [RESULT_ERROR, self::$alipayModel->getError()];
    }  
    /**
     * 会员删除
     */
    public function dingdanDel($where = [])
    {
        
      
        
        return self::$alipayModel->deleteInfo($where) ? [RESULT_SUCCESS, '订单删除成功'] : [RESULT_ERROR, self::$alipayModel->getError()];
    }
}
