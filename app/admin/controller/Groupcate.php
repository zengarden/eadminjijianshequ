<?php
// +----------------------------------------------------------------------
// | Author: Bigotry <3162875@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;


use app\common\logic\Common as LogicCommon;

/**
 * 小组分类控制器
 */
class Groupcate extends AdminBase
{
    
    /**
     * 小组分类逻辑
     */
	
    
    private static $commonLogic = null;
    /**
     * 构造方法
     */
    public function _initialize()
    {
        
        parent::_initialize();
        self::$commonLogic = get_sington_object('commonLogic', LogicCommon::class);
       
    }

    
    /**
     * 小组分类列表
     */
    public function groupcateList()
    {
        
        $where = self::$commonLogic->getWhere($this->param);
        
        $this->assign('list', self::$commonLogic->getDataList('groupcate',$where, true, 'id desc'));
       
       
        return $this->fetch('groupcate_list');
    }
    
    /**
     * 小组分类添加
     */
    public function groupcateAdd()
    {
        
        IS_POST && $this->jump(self::$commonLogic->dataAdd('groupcate',$this->param));
        
        return $this->fetch('groupcate_add');
    }
    /**
     * 小组分类编辑
     */
    public function groupcateEdit()
    {
    	$info = self::$commonLogic->getDataInfo('groupcate',['id' => $this->param['id']]);
    	IS_POST && $this->jump(self::$commonLogic->dataEdit('groupcate',$this->param,$info));
    	
    	
    	$this->assign('info', $info);
    	return $this->fetch('groupcate_edit');
    }
    /**
     * 小组分类批量删除
     */
    public function groupcateAlldel($ids = 0)
    {
    
    	$this->jump(self::$commonLogic->dataDel('groupcate',['id'=>array('in',$ids)]));
    }
    /**
     * 小组分类删除
     */
    public function groupcateDel($id = 0)
    {
        
        $this->jump(self::$commonLogic->dataDel('groupcate',['id' => $id]));
    }
    /**
     * 导航状态更新
     */
    public function groupcateCstatus($id = 0,$status)
    {
    
    	$this->jump(self::$commonLogic->setDataValue('groupcate',['id' => $id],'status',$status));
    }
}
