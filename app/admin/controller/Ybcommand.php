<?php
// +----------------------------------------------------------------------
// | Author: Bigotry <3162875@qq.com>
// +----------------------------------------------------------------------

namespace app\admin\controller;

use think\Controller;
use app\common\logic\Docxs as LogicDocxs;
use app\common\logic\Doccon as LogicDoccon;


class Ybcommand extends Controller
{
    
   public function copyfile($fileid){
   	
   	 $fileinfo = model('file')->where(['id'=>$fileid])   ->find();
   	
   	  copy(getfileurl_jd($fileid,0), PATH_UPLOAD.'doc'.DS.$fileinfo['savename']);
   	  
   	  
   	
   }
   //计划任务
   public function twentyfour(){
   
   	$docxsLogic = get_sington_object('docxsLogic', LogicDocxs::class);

   	$docxslist = $docxsLogic->getDocxsList(['m.status'=>1], true, 'm.id desc');
   
  
   	
   foreach ($docxslist as $k => $v){
   
   if(lefttime($v['create_time'])>0){
   	if($v['days']-lefttime($v['create_time'])>0){
   		 
   		 
   		 
   		$docxsLogic->setDocxsValue(['id'=>$v['id']],'days',$v['days']-lefttime($v['create_time']));
   		 
   	}else{
   		 
   		$data['id']=$v['id'];
   		$data['days']=0;
   		$data['status']=2;
   		$docxsLogic->setDocxsInfo($data);
   		//悬赏结束如果还没有一篇文档，则退还积分，还未写
   		
   		 
   		 
   	}
   }
   	

   	
   	}
   	
   	//预览是否存在
   	$docconLogic = get_sington_object('docconLogic', LogicDoccon::class);
   	
   	$docconlist = $docconLogic->getDocconList(['m.pageid'=>0,'m.status'=>1], true, 'm.id desc');
   	
   	
   	
   	foreach($docconlist as $k =>$v){
   		
   		$arr=explode('.'.$v['ext'], $v['savename']);
   		
   		
   		
   		if(file_exists(PATH_UPLOAD.'docview/Preview/'.$arr[0].'0001.jpg')){
   			
   			$ipstr=getipstr($v['ext'],$arr[0]);
   			
   			$realstrcount=substr_count($ipstr,'stl_01');
   			
   			//得到文档页数
   			 			
   			$docconLogic->setDocconValue(['id'=>$v['id']],'pageid',$realstrcount);
   			
   		}
   		
   		
   		
   		
   	}
   	
   
   }

}
