<?php
namespace app\index\controller;
use app\common\controller\HomeBase;
use app\common\logic\Common as LogicCommon;
use app\common\logic\User as LogicUser;


class User extends  HomeBase
{
	
	// 用户逻辑
	private static $logicUser = null;
	private static $commonLogic = null;
	
	
	public function _initialize()
	{
		parent::_initialize();
		
		self::$logicUser = get_sington_object('logicUser', LogicUser::class);
		self::$commonLogic = get_sington_object('commonLogic', LogicCommon::class);

		$uid=is_login();
		$this->assign('nowuid',$uid);
		if($uid>0){
			$nowuserinfo=self::$commonLogic->getDataInfo('user',['id'=>$uid]);
			$this->assign('nowuserinfo',$nowuserinfo);
		}
		
	}

	public function focususer(){
		
		
		$uid=is_login();
		if($uid==0){
			$this->jump(([RESULT_ERROR, '请登录后操作']));
		}else{
			
			$where['type']=0;
			$where['uid']=$uid;
			$where['sid']=$this->param['useruid'];
			
			if(model('zan')->where($where)->count()>0){
				$this->jump(self::$commonLogic->dataDel('zan',['type'=>0,'sid'=>$this->param['useruid'],'uid'=>$uid],'取消关注',true));
				
			}else{
				$data['type']=0;
				$data['sid']=$this->param['useruid'];
				$data['uid']=$uid;
				$this->jump(self::$commonLogic->dataAdd('zan',$data,false,'关注成功'));
			}
			
		
			
			
		}
	
		
		
	}

	public function userfocus(){
		!is_login() && $this->jump(RESULT_REDIRECT, 'Index/index');
		$uid=is_login();
		empty($this->param['type']) ? $type = 1 : $type = $this->param['type'];
		
		$sidarr=model('zan')->where(['uid'=>$uid,'type'=>0])->column('sid');//得到所有的我关注的人
		$gzidarr=model('zan')->where(['sid'=>$uid,'type'=>0])->column('uid');//得到所有关注我的人
		
		if($type==1){//好友
			
			$userlist = self::$commonLogic->getDataList('zan',['m.uid'=>array('in',$sidarr),'m.sid'=>$uid,'m.type'=>0],'m.uid,m.sid,m.create_time,user.nickname,user.id as userid,user.userhead,user.description,user.statusdes,user.grades,count(topic.id) as topiccount','m.create_time desc',8,[['user','user.id=m.uid'],['topic','topic.uid=m.uid','LEFT']],'topic.uid');
		
		}
		if($type==2){//关注
			
			$userlist = self::$commonLogic->getDataList('zan',['m.uid'=>$uid,'m.type'=>0,'m.sid'=>array('not in',$gzidarr)],'m.uid,m.sid,m.create_time,user.nickname,user.id as userid,user.userhead,user.description,user.statusdes,user.grades,count(topic.id) as topiccount','m.create_time desc',8,[['user','user.id=m.sid'],['topic','topic.uid=m.sid','LEFT']],'topic.uid');
			
		}
		if($type==3){//粉丝
			$userlist = self::$commonLogic->getDataList('zan',['m.sid'=>$uid,'m.type'=>0,'m.uid'=>array('not in',$sidarr)],'m.uid,m.sid,m.create_time,user.nickname,user.id as userid,user.userhead,user.description,user.statusdes,user.grades,count(topic.id) as topiccount','m.create_time desc',8,[['user','user.id=m.uid'],['topic','topic.uid=m.uid','LEFT']],'topic.uid');
			
		}
		$this->assign('uid',$uid);
		$this->assign('type',$type);
		$this->assign('userlist',$userlist);
		
		return $this->fetch();
	}

	public function home(){

		
		if(empty($this->param['id'])){
			$this->error('参数错误',url('index/index'));
		}else{
			$useruid=$this->param['id'];
			
			//参加的小组
		$joingrouplist = self::$commonLogic->getDataList('user_group',['m.uid'=>$useruid],'m.group_id,group.name,m.create_time,group.cover_id','m.create_time desc',false,[['group','group.id=m.group_id','LEFT']]);
		$this->assign('joingrouplist',$joingrouplist);
		
		//发表的帖子
		
		$list=self::$commonLogic->getDataList('topic',['m.status'=>1,'m.uid'=>$useruid],'m.*,user.nickname,user.userhead','m.create_time desc',0,[['user','user.id=m.uid','LEFT']]);
		foreach ($list as $k =>$v){
			 
			$comment=model('comment')->where(['fid'=>$v['id']])->order('create_time desc')->limit(1)->select();
			if($comment){
				$list[$k]['ccreate_time']=$comment[0]['create_time'];
				$list[$k]['cuid']=$comment[0]['uid'];
			}
			 
		}
		
		    $this->assign('list',$list);
		
		
			
			$uid=is_login();
			usercz($uid,$useruid,2,3);
			
			$newvisitorlist=self::$commonLogic->getDataList('usercz',['m.did'=>$useruid,'m.type'=>2,'m.cid'=>3],'m.uid,user.nickname,user.userhead','m.create_time desc',false,[['user','user.id=m.uid','LEFT']],'m.uid',9);
			$this->assign('newvisitorlist',$newvisitorlist);
			
			$userinfo=self::$commonLogic->getDataInfo('user',['id'=>$useruid]);
			
			
			$userinfo['topiccount']=model('topic')->where(['uid'=>$useruid])->count();
			
			$userinfo['gzusercount']=model('zan')->where(['type'=>0,'sid'=>$useruid])->count();
			
			$userinfo['fsusercount']=model('zan')->where(['uid'=>$useruid,'type'=>0])->count();
			
			$userinfo['groupcount']=model('user_group')->where(['uid'=>$useruid])->count();
			
			
			$this->assign('userinfo',$userinfo);
			
			$this->assign('useruid',$useruid);
			
		
			if($uid==$useruid){
				$hasfocus=2;
			}else{
				if(model('zan')->where(['uid'=>$uid,'sid'=>$useruid,'type'=>0])->count()>0){
					$hasfocus=1;
				}else{
					$hasfocus=0;
				}
			}
			$this->assign('hasfocus',$hasfocus);
		}
		
		
			
		return $this->fetch();
			
	}
   public function index(){
   	!is_login() && $this->jump(RESULT_REDIRECT, 'Index/index');


   
   
   	return $this->fetch();
   	
   }
   /**
    * 修改个人头像处理
    */
   public function setavatarHandle(){
   	
   	$info=session('member_info');
   	$data=$this->param;
   	$data['id']=$info['id'];
   
   	$obj=new User();
   	$this->jump(self::$commonLogic->dataEdit('user',$data,false,$info='信息编辑成功',$obj,'callback_setinfo'));
   }
   
   /**
    * 修改个人信息处理
    */
   public function setinfoHandle()
   {
   	
   
   	$info=session('member_info');
   	
   	$data=$this->param;
   	$data['username']=$info['username'];
   	$data['id']=$info['id'];
   	
   	$obj=new User();
   	
   	$this->jump(self::$commonLogic->dataEdit('user',$data,true,$info='信息编辑成功',$obj,'callback_setinfo'));
   	 
   }
   public function callback_setinfo($result,$data){
   	$member=self::$commonLogic->getDataInfo('user',['id'=>$data['id']]);
   	session('member_info', $member);
   
   }
   /**
    * 修改密码处理
    */
   public function setpasswordHandle()
   {
   	$data=$this->param;
   	$info=session('member_info');
   	$this->jump(self::$logicUser->setMemberPassword($data, $info));
   	 
   }
   public function mess(){
   	!is_login() && $this->jump(RESULT_REDIRECT, 'Index/index');
   	$uid=is_login();
   	$midarr=model('readmessage')->where(['uid'=>$uid])->column('mid');
   	$list=self::$commonLogic->getDataList('message',['id'=>array('not in',$midarr),'touid'=>array('in',array(0,$uid)),'status'=>1],true,'update_time desc');
   	
   	$this->assign('list',$list);
   	return $this->fetch();
   
   }
public function ajaxdelmess(){
	$myuid=is_login();
	$id=$this->param['id'];
	$uid=$this->param['uid'];
	if($uid>0){
		
		$where['id']=$id;
		
		$this->jump(self::$commonLogic->dataDel('message',$where,'删除成功',true));
	}else{
		$data['uid']=$myuid;
		$data['mid']=$id;
		$this->jump(self::$commonLogic->dataAdd('readmessage',$data,false,'删除成功'));
	}
	
}
public function ajaxdelallmess(){
	$uid=is_login();
	$midarr=model('readmessage')->where(['uid'=>$uid])->column('mid');
	self::$commonLogic->dataDel('message',['id'=>array('not in',$midarr),'touid'=>$uid],'',true);//删除私信
	
	$list=self::$commonLogic->getDataList('message',['id'=>array('not in',$midarr),'touid'=>0],true,'update_time desc',false);
	foreach ($list as $k =>$v){
		$data['uid']=$uid;
		$data['mid']=$v['id'];
		$n=self::$commonLogic->dataInsert('readmessage',$data,false,'删除成功');
		
		
	}
	
	$this->jump([RESULT_SUCCESS, '清空成功']);
	
}



   public function shoucang(){
   	!is_login() && $this->jump(RESULT_REDIRECT, 'Index/index');
   	$uid=is_login();
 
   	empty($this->param['type']) ? $type = 1 : $type = $this->param['type'];//1表示帖子
   	if($type==1){
   		$topiclist = self::$commonLogic->getDataList('zan',['m.uid'=>$uid,'m.type'=>3],'m.*,topic.title,topic.choice,topic.settop,topic.reply,topic.view,topic.create_time as topiccreate_time,user.nickname,user.userhead,group.name','m.create_time desc',8,[['topic','topic.id=m.sid'],['group','group.id=topic.tid'],['user','user.id=topic.uid']]);
   		
   	}else{

   		
   		
   	}
   	$this->assign('type',$type);
   	$this->assign('topiclist',$topiclist);
   	
   	return $this->fetch();
   	 
   }
   public function mygroup(){
   	!is_login() && $this->jump(RESULT_REDIRECT, 'Index/index');
   	$uid=is_login();
   
   	empty($this->param['type']) ? $type = 1 : $type = $this->param['type'];//1表示建立的2表示关注的
   	if($type==1){
   		$grouplist = self::$commonLogic->getDataList('user_group',['m.uid'=>$uid,'m.grade'=>2],'m.*,group.cover_id,group.membercount,group.topiccount,group.name','m.create_time desc',8,[['group','group.id=m.group_id']]);
   		 
   	}else{
   
   		$grouplist = self::$commonLogic->getDataList('user_group',['m.uid'=>$uid,'m.grade'=>array('neq',2)],'m.*,group.cover_id,group.membercount,group.topiccount,group.name','m.create_time desc',8,[['group','group.id=m.group_id']]);
   		 
   	}
   	$this->assign('type',$type);
   	$this->assign('grouplist',$grouplist);
   
   	return $this->fetch();
   	 
   }
   public function mytopic(){
   	!is_login() && $this->jump(RESULT_REDIRECT, 'Index/index');
   	$uid=is_login();
 
   
   
   		$topiclist = self::$commonLogic->getDataList('topic',['m.uid'=>$uid,'m.status'=>1],'m.*,user.nickname,user.userhead,group.name','m.create_time desc',8,[['group','group.id=m.tid'],['user','user.id=m.uid']]);
   		
  
   
   	$this->assign('topiclist',$topiclist);
   	
   	return $this->fetch();
   	 
   }

   /**
    * 注册页面
    */
   public function register(){

   	is_login() && $this->jump(RESULT_REDIRECT, 'Index/index');
   	
   	$yzm_list = parse_config_array('yzm_list');//1\注册2\登录3\忘记密码4\后台登录
   	 
   	if(in_array(1, $yzm_list)){
   		 
   		$yzm=1;
   		 
   	}else{
   		 
   		$yzm=0;
   		 
   	}
   	
   	$this->assign('yzm',$yzm);
   	
   	return $this->fetch();
   	 
   }
   /**
    * 注册处理
    */
   public function regHandle($username = '', $password = '', $repassword = '',$usermail = '', $verify = '')
   {
   	 
   	$this->jump(self::$logicUser->regHandle($username, $password, $repassword,$usermail, $verify));
   
   }   
   
   
   
   
   /**
    * 登录页面
    */
   public function login(){
   	
   	is_login() && $this->jump(RESULT_REDIRECT, 'Index/index');
   	
   	$yzm_list = parse_config_array('yzm_list');//1\注册2\登录3\忘记密码4\后台登录
   	
   	if(in_array(2, $yzm_list)){
   		
   		$yzm=1;
   		
   	}else{
   		
   		$yzm=0;
   		
   	}
   	
   	$this->assign('yzm',$yzm);
   	
   	return $this->fetch();
   	 
   }
   /**
    * 登录处理
    */
   public function loginHandle($username = '', $password = '', $verify = '')
   {
   	 
   	$this->jump(self::$logicUser->loginHandle($username, $password, $verify));
   	
   }
   /**
    * 注销处理
    */
   public function logout()
   {
   	 
   	$this->jump(self::$logicUser->logout());
   
   }
   
}
